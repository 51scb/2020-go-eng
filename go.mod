module github.com/51scb/2020-go-eng

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/kirinlabs/HttpRequest v1.1.1
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.26.0
)

package main

import (
	"fmt"
	"net"
	"sync"
	"time"

	"github.com/51scb/2020-go-eng/microservice/08_stream_grpc/constants"
	"github.com/51scb/2020-go-eng/microservice/08_stream_grpc/proto"
	"google.golang.org/grpc"
)

type Server struct{}

func (s *Server) GetStream(req *proto.StreamReqData, res proto.Greeter_GetStreamServer) error {
	i := 0
	for {
		if err := res.Send(&proto.StreamResData{
			Data: fmt.Sprintf("%v-%q", time.Now().Unix(), req.Data),
		}); err != nil {
			continue
		}
		time.Sleep(time.Second)
		if i > 10 {
			break
		}
		i++
	}
	return nil
}

func (s *Server) PutStream(clientStream proto.Greeter_PutStreamServer) error {
	for {
		if req, err := clientStream.Recv(); err != nil {
			fmt.Println(err)
			break
		} else {
			fmt.Println(req.Data)
		}
	}
	return nil
}

func (s *Server) AllStream(allStream proto.Greeter_AllStreamServer) error {
	wg := sync.WaitGroup{}
	go func() {
		wg.Add(1)
		defer wg.Done()
		for {
			req, err := allStream.Recv()
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Println("收到客户端消息：", req.Data)
		}
	}()
	go func() {
		wg.Add(1)
		defer wg.Done()
		for {
			if err := allStream.Send(&proto.StreamResData{
				Data: fmt.Sprintf("%v", time.Now()),
			}); err != nil {
				fmt.Println(err)
				continue
			}
			time.Sleep(time.Second)
		}
	}()
	wg.Wait()
	return nil
}

func main() {
	s := grpc.NewServer()
	proto.RegisterGreeterServer(s, &Server{})

	listener, err := net.Listen("tcp", constants.ADDR)
	if err != nil {
		panic("listen failed:" + err.Error())
	}
	if err := s.Serve(listener); err != nil {
		panic("serve failed:" + err.Error())
	}
}

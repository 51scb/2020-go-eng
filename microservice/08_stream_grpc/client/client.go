package main

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/51scb/2020-go-eng/microservice/08_stream_grpc/constants"
	"github.com/51scb/2020-go-eng/microservice/08_stream_grpc/proto"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial(constants.ADDR, grpc.WithInsecure())
	if err != nil {
		panic("Dial failed:" + err.Error())
	}
	defer conn.Close()

	c := proto.NewGreeterClient(conn)
	//getStream(c)
	//putStream(c)
	allStream(c)
}

// getStream 服务端流模式
func getStream(c proto.GreeterClient) {
	client, err := c.GetStream(context.Background(), &proto.StreamReqData{
		Data: "51scb-server-stream",
	})
	if err != nil {
		panic("GetStream failed:" + err.Error())
	}
	for {
		res, err := client.Recv()
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Println(res.Data)
	}
}

// putStream 客户端流模式
func putStream(c proto.GreeterClient) {
	client, err := c.PutStream(context.Background())
	if err != nil {
		panic("PutStream failed:" + err.Error())

	}
	for i := 0; i < 10; i++ {
		if err := client.Send(&proto.StreamReqData{
			Data: fmt.Sprintf("PutStream-%v", time.Now()),
		}); err != nil {
			fmt.Println(err)
			continue
		}
		time.Sleep(time.Second)
	}
}
func allStream(c proto.GreeterClient) {
	client, err := c.AllStream(context.Background())
	if err != nil {
		panic("AllStream failed:" + err.Error())
	}
	var wg sync.WaitGroup
	go func() {
		wg.Add(1)
		defer wg.Done()
		for {
			if err := client.Send(&proto.StreamReqData{
				Data: fmt.Sprintf("allStream - %v", time.Now()),
			}); err != nil {
				fmt.Println(err)
				break
			}
			time.Sleep(time.Second)
		}
	}()
	go func() {
		wg.Add(1)
		defer wg.Done()
		for {
			req, err := client.Recv()
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Println(req.Data)
		}
	}()
	wg.Wait()
}

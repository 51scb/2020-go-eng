package main

import (
	"log"
	"net"
	"net/rpc"
)

type HelloService struct{}

func (hs *HelloService) Hello(request string, reply *string) error {
	*reply = "👋" + request
	return nil
}

func main() {
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal(err)
	}
	if err := rpc.RegisterName("HelloService", &HelloService{}); err != nil {
		log.Fatal(err)
	}
	conn, err := listener.Accept()
	if err != nil {
		log.Fatal(err)
	}
	rpc.ServeConn(conn)
}

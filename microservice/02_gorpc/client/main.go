package main

import (
	"log"
	"net/rpc"
)

func main() {
	client, err := rpc.Dial("tcp", "127.0.0.1:1234")
	if err != nil {
		log.Fatal(err)
	}
	var reply string
	if err := client.Call("HelloService.Hello", "猪头", &reply); err != nil {
		log.Fatal(err)
	}
	log.Println("调用结果：", reply)
}

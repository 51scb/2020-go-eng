package main

import (
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type HelloService struct{}

func (hs *HelloService) Hello(request string, reply *string) error {
	*reply = "Hello, " + request
	return nil
}
func main() {
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		panic(err)
	}
	if err := rpc.RegisterName("HelloService", &HelloService{}); err != nil {
		panic(err)
	}

	for {

		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}

		go rpc.ServeCodec(jsonrpc.NewServerCodec(conn))
	}
}

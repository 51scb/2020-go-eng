package main

import (
	"fmt"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:1234")
	if err != nil {
		panic(err)
	}

	var reply string

	client := rpc.NewClientWithCodec(jsonrpc.NewClientCodec(conn))
	if err := client.Call("HelloService.Hello", "51scb", &reply); err != nil {
		panic(err)
	}
	/*
		{"method":"HelloService.Hello", "params":["hello"],"id":0}
	*/

	fmt.Println(reply)
}

import json
import socket

request = {
    "method":"HelloService.Hello",
    "params":["51scb-py"],
    "id":0
}

client = socket.create_connection(('127.0.0.1', 1234))
client.sendall(json.dumps(request).encode())

rsp = client.recv(1024)
rsp = json.loads(rsp.decode())
print(rsp)

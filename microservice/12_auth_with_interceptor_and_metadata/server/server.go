package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/51scb/2020-go-eng/microservice/12_auth_with_interceptor_and_metadata/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type Server struct{}

func (s *Server) SayHello(ctx context.Context, req *proto.Request) (*proto.Reply, error) {
	return &proto.Reply{
		Message: "你好，" + req.Name,
	}, nil
}

func interceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handerl grpc.UnaryHandler) (resp interface{}, err error) {
	fmt.Println("接收到了新请求", time.Now())
	unauth := status.Error(codes.Unauthenticated, "未授权用户")
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, unauth
	}
	var (
		appId, secert string
	)
	if appIdSlice, ok := md["appid"]; ok {
		appId = appIdSlice[0]
	}
	if secertSlice, ok := md["secert"]; ok {
		secert = secertSlice[0]
	}
	if !(appId == "123456" && secert == "foobar") {
		return nil, unauth
	}

	res, err := handerl(ctx, req)
	fmt.Println("请求已经完成")
	return res, err
}
func main() {
	opt := grpc.UnaryInterceptor(interceptor)
	g := grpc.NewServer(opt)
	proto.RegisterGreeterServer(g, &Server{})
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	if err := g.Serve(lis); err != nil {
		log.Fatal(err)
	}
}

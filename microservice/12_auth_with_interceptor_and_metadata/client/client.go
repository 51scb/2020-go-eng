package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/51scb/2020-go-eng/microservice/12_auth_with_interceptor_and_metadata/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type customCredential struct{}

func (c *customCredential) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"appid":  "123456",
		"secert": "foobar",
	}, nil
}
func (c *customCredential) RequireTransportSecurity() bool {
	return false
}

func interceptor(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	start := time.Now()
	ctx = metadata.NewOutgoingContext(ctx, metadata.Pairs(
		"appid", "123456",
		"secert", "foobar111",
	))
	err := invoker(ctx, method, req, reply, cc, opts...)
	if err != nil {
		return err
	}
	fmt.Println("耗时：", time.Since(start))
	return nil
}
func main() {
	//opt := grpc.WithUnaryInterceptor(interceptor)
	opt := grpc.WithPerRPCCredentials(&customCredential{})
	conn, err := grpc.Dial("127.0.0.1:8080", grpc.WithInsecure(), opt)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	c := proto.NewGreeterClient(conn)
	reply, err := c.SayHello(context.Background(), &proto.Request{
		Name: "51scb",
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(reply.Message)
}

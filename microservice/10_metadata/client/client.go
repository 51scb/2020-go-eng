package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/51scb/2020-go-eng/microservice/10_metadata/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	md := metadata.Pairs(
		"timestamp", time.Now().String(),
		"email", "51scb@mail.com",
		"password", "foobar",
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)

	c := proto.NewGreeterClient(conn)
	reply, err := c.SayHello(ctx, &proto.Request{
		Name: "51scb",
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(reply.Message)
}

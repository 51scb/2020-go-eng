package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"strings"

	"github.com/51scb/2020-go-eng/microservice/10_metadata/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type Server struct{}

func (s *Server) SayHello(ctx context.Context, req *proto.Request) (*proto.Reply, error) {
	msg := make([]string, 0)
	msg = append(msg, fmt.Sprintf("Hello, %s", req.Name))
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		for k, v := range md {
			msg = append(msg, fmt.Sprintf("%s: %s", k, strings.Join(v, ",")))
		}
	}

	return &proto.Reply{
		Message: strings.Join(msg, "\n"),
	}, nil
}

func main() {
	g := grpc.NewServer()
	proto.RegisterGreeterServer(g, &Server{})
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}

	if err := g.Serve(lis); err != nil {
		log.Fatal(err)
	}
}

package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/51scb/2020-go-eng/microservice/11_interceptor/proto"
	"google.golang.org/grpc"
)

type Server struct{}

func (s *Server) SayHello(ctx context.Context, req *proto.Request) (*proto.Reply, error) {
	return &proto.Reply{
		Message: "你好，" + req.Name,
	}, nil
}

func interceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handerl grpc.UnaryHandler) (resp interface{}, err error) {
	fmt.Println("接收到了新请求", time.Now())
	res, err := handerl(ctx, req)
	fmt.Println("请求已经完成")
	return res, err
}
func main() {
	opt := grpc.UnaryInterceptor(interceptor)
	g := grpc.NewServer(opt)
	proto.RegisterGreeterServer(g, &Server{})
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	if err := g.Serve(lis); err != nil {
		log.Fatal(err)
	}
}

package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/51scb/2020-go-eng/microservice/11_interceptor/proto"
	"google.golang.org/grpc"
)

func interceptor(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	start := time.Now()
	err := invoker(ctx, method, req, reply, cc, opts...)
	fmt.Println("耗时：", time.Since(start))
	return err
}
func main() {
	opt := grpc.WithUnaryInterceptor(interceptor)
	conn, err := grpc.Dial("127.0.0.1:8080", grpc.WithInsecure(), opt)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	c := proto.NewGreeterClient(conn)
	reply, err := c.SayHello(context.Background(), &proto.Request{
		Name: "51scb",
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(reply.Message)
}

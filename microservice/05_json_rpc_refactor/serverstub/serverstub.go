package serverstub

import "net/rpc"

type Service interface {
	Name() string
}

func RegisterService(srv Service) error {
	return rpc.RegisterName(srv.Name(), srv)
}

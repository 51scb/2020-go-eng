package main

import (
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"

	"github.com/51scb/2020-go-eng/microservice/05_json_rpc_refactor/handler"
	"github.com/51scb/2020-go-eng/microservice/05_json_rpc_refactor/serverstub"
)

func main() {
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		panic(err)
	}
	if err := serverstub.RegisterService(&handler.HelloService{}); err != nil {
		panic(err)
	}

	for {

		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}

		go rpc.ServeCodec(jsonrpc.NewServerCodec(conn))
	}
}

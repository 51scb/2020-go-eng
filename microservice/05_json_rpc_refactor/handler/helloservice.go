package handler

type HelloService struct{}

func (hs *HelloService) Hello(request string, reply *string) error {
	*reply = "Hello, " + request
	return nil
}

func (hs *HelloService) Name() string {
	return HelloServiceName
}

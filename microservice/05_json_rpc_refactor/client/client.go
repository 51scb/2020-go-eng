package main

import (
	"fmt"

	"github.com/51scb/2020-go-eng/microservice/05_json_rpc_refactor/clientstub"
)

func main() {
	client := clientstub.NewHelloServiceClient("tcp", "127.0.0.1:1234")
	var reply string
	if err := client.Hello("51scb", &reply); err != nil {
		panic(err)
	}
	fmt.Println(reply)
}

package clientstub

import (
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"

	"github.com/51scb/2020-go-eng/microservice/05_json_rpc_refactor/handler"
)

type HelloServiceStub struct {
	*rpc.Client
}

func NewHelloServiceClient(protol, address string) *HelloServiceStub {
	conn, err := net.Dial(protol, address)
	if err != nil {
		panic(err)
	}
	client := rpc.NewClientWithCodec(jsonrpc.NewClientCodec(conn))
	return &HelloServiceStub{
		client,
	}
}
func (s *HelloServiceStub) Hello(request string, reply *string) error {
	err := s.Call(handler.HelloServiceName+".Hello", request, reply)
	if err != nil {
		return err
	}
	return nil
}

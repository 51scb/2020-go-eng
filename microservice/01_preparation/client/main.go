package main

import (
	"encoding/json"
	"fmt"

	"github.com/kirinlabs/HttpRequest"
)

type ResponseData struct {
	Data int `json:"data"`
}

func Add(a, b int) int {
	req := HttpRequest.NewRequest()
	res, _ := req.Get(fmt.Sprintf("http://127.0.0.1:8000/add?a=%d&b=%d", a, b))
	body, _ := res.Body()
	var data ResponseData
	json.Unmarshal(body, &data)
	return data.Data
}

func main() {
	fmt.Println(Add(12, 34))
}

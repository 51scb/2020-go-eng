package main

import "fmt"

func Add(a, b int) int {
	return a + b
}

type Company struct {
	Name    string
	Address string
}

type Employee struct {
	Name    string
	Company Company
}
type PrintResult struct {
	Info string
	Err  error
}

func RpcPrintln(empl *Employee) *PrintResult {
	return nil
}
func main() {
	fmt.Println(Employee{
		Name: "51scb",
		Company: Company{
			Name:    "火星人传媒",
			Address: "火星",
		},
	})
}

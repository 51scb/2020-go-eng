package main

import (
	"context"
	"net"

	"github.com/51scb/2020-go-eng/microservice/07_grpc/proto"
	"google.golang.org/grpc"
)

type Server struct{}

func (s *Server) SayHello(ctx context.Context, request *proto.HelloRequest) (*proto.HellpReply, error) {
	return &proto.HellpReply{
		Message: "Hello, " + request.Name,
	}, nil
}

func main() {
	g := grpc.NewServer()
	proto.RegisterGreeterServer(g, &Server{})

	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		panic("failed to listen:" + err.Error())
	}
	if err := g.Serve(listener); err != nil {
		panic("failed to serve:" + err.Error())
	}
}

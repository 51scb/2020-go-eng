package main

import (
	"context"
	"fmt"

	"github.com/51scb/2020-go-eng/microservice/07_grpc/proto"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:8080", grpc.WithInsecure())
	if err != nil {
		panic("dial failed:" + err.Error())
	}
	defer conn.Close()

	c := proto.NewGreeterClient(conn)
	r, err := c.SayHello(context.Background(), &proto.HelloRequest{
		Name: "51scb-grpc",
	})
	if err != nil {
		panic("call failed:" + err.Error())
	}
	fmt.Println(r.Message)
}

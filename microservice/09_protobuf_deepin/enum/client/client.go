package main

import (
	"context"
	"fmt"
	"log"

	"github.com/51scb/2020-go-eng/microservice/09_protobuf_deepin/enum/proto"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatal("Dial failed:", err)
	}

	c := proto.NewGreeterClient(conn)
	res, err := c.SayHello(context.Background(), &proto.Request{
		Name:   "51scb",
		Gender: proto.Gender_MALE,
	})
	if err != nil {
		log.Fatal("SayHello failed:", err)
	}
	fmt.Println(res.Message)

}

package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/51scb/2020-go-eng/microservice/09_protobuf_deepin/enum/proto"
	"google.golang.org/grpc"
)

type Server struct {
}

func (s *Server) SayHello(ctx context.Context, req *proto.Request) (*proto.Replay, error) {
	msg := fmt.Sprintf("%q, %v", req.Name, req.Gender)
	return &proto.Replay{
		Message: msg,
	}, nil
}

func main() {
	g := grpc.NewServer()
	proto.RegisterGreeterServer(g, &Server{})

	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal("listen failed:", err)
	}
	if err := g.Serve(lis); err != nil {
		log.Fatal("serve failed:", err)
	}
}

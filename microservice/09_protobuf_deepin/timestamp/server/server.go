package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/51scb/2020-go-eng/microservice/09_protobuf_deepin/timestamp/proto"
	"google.golang.org/grpc"
)

type Server struct{}

func (s *Server) SayHello(ctx context.Context, req *proto.Request) (*proto.Reply, error) {
	return &proto.Reply{
		Message: fmt.Sprintf("%s, %v", req.Name, req.Dateline.AsTime()),
	}, nil
}

func main() {
	r := grpc.NewServer()
	proto.RegisterGreeterServer(r, &Server{})
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	if err := r.Serve(lis); err != nil {
		log.Fatal(err)
	}
}

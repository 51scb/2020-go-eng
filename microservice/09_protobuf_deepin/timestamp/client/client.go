package main

import (
	"context"
	"log"

	"github.com/51scb/2020-go-eng/microservice/09_protobuf_deepin/timestamp/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	c := proto.NewGreeterClient(conn)
	reply, err := c.SayHello(context.Background(), &proto.Request{
		Name:     "",
		Dateline: timestamppb.Now(),
	})
	log.Println(reply.Message)
}

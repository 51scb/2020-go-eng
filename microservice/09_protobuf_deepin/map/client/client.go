package main

import (
	"context"
	"log"

	"github.com/51scb/2020-go-eng/microservice/09_protobuf_deepin/map/proto"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	c := proto.NewGreeterClient(conn)
	reply, err := c.SayHello(context.Background(), &proto.Request{
		Name: "51scb",
		Scores: map[string]uint32{
			"张三": 99,
			"李四": 55,
			"王五": 44,
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Println(reply.Message)
}

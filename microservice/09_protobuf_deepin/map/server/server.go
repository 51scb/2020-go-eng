package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"strings"

	"github.com/51scb/2020-go-eng/microservice/09_protobuf_deepin/map/proto"
	"google.golang.org/grpc"
)

type Server struct{}

func (s *Server) SayHello(ctx context.Context, req *proto.Request) (*proto.Reply, error) {
	sb := strings.Builder{}
	for name, score := range req.Scores {
		sb.WriteString(fmt.Sprintf("%s: %d", name, score))
		sb.WriteString("\n")
	}
	return &proto.Reply{
		Message: fmt.Sprintf("班长：%s, 成绩：%s", req.Name, sb.String()),
	}, nil
}

func main() {
	r := grpc.NewServer()
	proto.RegisterGreeterServer(r, &Server{})

	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	if err := r.Serve(lis); err != nil {
		log.Fatal(err)
	}
}

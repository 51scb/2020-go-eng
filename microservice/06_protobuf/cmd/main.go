package main

import (
	"fmt"

	"github.com/51scb/2020-go-eng/microservice/06_protobuf/helloworld"
	"github.com/golang/protobuf/proto"
)

func main() {
	req := helloworld.HelloRequest{
		Name:    "51scb",
		Age:     18,
		Courses: []string{"go", "gin", "微服务"},
	}
	rsp, err := proto.Marshal(&req)
	if err != nil {
		panic(err)
	}
	//fmt.Println(string(rsp))
	var newReq helloworld.HelloRequest
	if err := proto.Unmarshal(rsp, &newReq); err != nil {
		panic(err)
	}
	fmt.Printf("Name: %v, Age: %v, Courses: %v\n", newReq.Name, newReq.Age, newReq.Courses)
}

# 电商项目：微服务基础

## rpc 开发四要素

- 客户端(client)：服务调用发起方，也称为服务消费者

- 客户端存根(client stub)：该程序运行在客户端所在的计算机上，主要用来存储要调用的服务器地址。另外还负责将客户端请求远端服务器程序的数据信息打包成数据包，通过网络发送给服务端存根；其次，还要接收服务端存根程序发送的调用结果数据包，并解析返回给客户端

- 服务端(server)：远端的计算机上运行的程序，其中有客户端要调用的方法

- 服务端存根(server stub)：接收客户端存根通过网络发送的请求消息数据包，并调用服务端中真正的程序功能方法，完成功能调用；其次，将服务端执行调用的结果进行数据处理打包发送给客户端存根程序

## go 语言的 rpc 序列化和反序列化的协议

Gob

## protobuf

是 Google 出品的一种轻量、高效的结构化数据存储格式。[官网](https://developers.google.com/protocol-buffers)

### 安装

- protoc

[github release](https://github.com/protocolbuffers/protobuf/releases)

或

```bash
brew install protobuf
```

- go 依赖包

```bash
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
```

或

```bash
brew install protoc-gen-go
```

- go 代码依赖

```bash
go get github.com/golang/protobuf/proto

```
